import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}


WORDLIST_FILENAME = "words.txt"

def loadWords():
    print("Loading word list from file...")
    inFile = open(WORDLIST_FILENAME, 'r')
    wordList = []
    for line in inFile:
        wordList.append(line.strip().lower())
    print("  ", len(wordList), "words loaded.")
    return wordList

def getFrequencyDict(sequence):
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq
	

def getWordScore(word, n):
    score = 0
    for char in word :
        score = score + SCRABBLE_LETTER_VALUES[char]
    score = score * len(word)
    if(len(word) == n) :
        score += 50
    return score


def displayHand(hand):
    for letter in hand.keys():
        for j in range(hand[letter]):
             print(letter,end=" ")       
    print()                             

def dealHand(n):
    hand={}
    numVowels = n // 3
    
    for i in range(numVowels):
        x = VOWELS[random.randrange(0,len(VOWELS))]
        hand[x] = hand.get(x, 0) + 1
        
    for i in range(numVowels, n):    
        x = CONSONANTS[random.randrange(0,len(CONSONANTS))]
        hand[x] = hand.get(x, 0) + 1
        
    return hand

def updateHand(hand, word):
    newHand = hand.copy()
    for letter in word :
        newHand[letter] -= 1
    return newHand


def isValidWord(word, hand, wordList):
    copyHand = hand.copy()
    for letter in word :
        if not letter in copyHand :
            return False
        elif copyHand[letter] == 0 :
            return False
        else :
            copyHand[letter] -= 1
    if not word in wordList :
        return False
    return True

def calculateHandlen(hand):
    handLen = 0
    for letter in hand :
        handLen += hand[letter]
    return handLen



def playHand(hand, wordList, n):
    score = 0
    while calculateHandlen(hand) > 0 :
        print("Current Hand:",end=' ')
        displayHand(hand)
        word = input('Enter word, or a "." to indicate that you are finished: ')
        if word == "." :
            break
        else :
            if not isValidWord(word, hand, wordList) :
                print("Invalid word, please try again.",end="\n\n")
            else :
                print('"' + word + '" earned you ' + str(getWordScore(word, n)) + ' points. Total: ' + str(score + getWordScore(word, n)) + ' points.')
                score += getWordScore(word, n)
                hand = updateHand(hand, word)
    if calculateHandlen(hand) == 0 :
        print("Run out of letters. Total score: " + str(score) + " points. ")
    else :
        print("Goodbye! Total score: " + str(score) + " points. ")
