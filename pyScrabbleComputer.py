from pyScrabble import *
import time

def compChooseWord(hand, wordList, n):
    bestScore = 0
    bestWord = None
    for word in wordList:
        if isValidWord(word, hand, wordList):
            score = getWordScore(word, n)
            if (score > bestScore):
                bestScore = score
                bestWord = word
    return bestWord

def compPlayHand(hand, wordList, n):
    totalScore = 0
    while (calculateHandlen(hand) > 0) :
        print("Current Hand: ", end=' ')
        displayHand(hand)
        word = compChooseWord(hand, wordList, n)
        if word == None:
            break            
        else :
            if (not isValidWord(word, hand, wordList)) :
                print('This is a terrible error! I need to check my own code!')
                break
            else :
                score = getWordScore(word, n)
                totalScore += score
                print('"' + word + '" earned ' + str(score) + ' points. Total: ' + str(totalScore) + ' points')              
                hand = updateHand(hand, word)
                print()
    print('Total score: ' + str(totalScore) + ' points.')

    
def playGame(wordList):
    hand = {}
    n = HAND_SIZE
    inp = input("Enter n to deal a new hand, r to replay the last hand, or e to end game: ")
    while inp != 'e' :
        if inp == 'r' :
            if not len(hand) == 0 :
                newinp = input("Enter u to have yourself play, c to have the computer play: ")
                if newinp == 'u' :
                    playHand(hand,wordList,n)
                elif newinp == 'c' :
                    compPlayHand(hand,wordList,n)
                else :
                    print("Invalid command.")
            else :
                print("You have not played a hand yet. Please play a new hand first!")
        elif inp == 'n' :
            hand = dealHand(n)
            newinp = input("Enter u to have yourself play, c to have the computer play: ")
            if newinp == 'u' :
                playHand(hand,wordList,n)
            elif newinp == 'c' :
                compPlayHand(hand,wordList,n)
            else :
                print("Invalid command.")
        elif inp == 'c' :
            if not len(hand) == 0 :
                compPlayHand(hand,wordList,n)
            else :
                hand = dealHand(n)
                compPlayHand(hand,wordList,n)
        elif inp == 'u' :
            if not len(hand) == 0 :
                playHand(hand,wordList,n)
            else :
                hand = dealHand(n)
                playHand(hand,wordList,n)
        else :
            print("Invalid command.")
        inp = input("Enter n to deal a new hand, r to replay the last hand, or e to end game: ")

if __name__ == '__main__':
    wordList = loadWords()
    playGame(wordList)


