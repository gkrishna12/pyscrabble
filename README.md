# README #

### What is this repository for? ###

* A simple game of Scrabble in Python.
* Edit 1 : Added functionality for Computer to play the game.

### How do I get set up? ###

* Download the files in a folder.
* make sure both the files are in the same folder or the console may throw an error on startup.
* Run pyScrabbleComputer.py and the game will start on the python console.

### Who do I talk to? ###

* K Gopal Krishna - mail@kgopalkrishna.com